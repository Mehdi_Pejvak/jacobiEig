
#####################################################################
# @name   Allrun
# @info   The bash file used to run code developed for implementing
#         iterative method to find eigenvalues and eigenvector of 
#         symmetric matrices. It clone "Eigen" library, compile
#         source codes and c++ test file. The test.cc file generated 
#         to test the implemented algorithm.
#
# @author Mehdi Baba Mehdi
# @date   12.07.2020
######################################################################

#!/bin/bash

EXE=test
RES=$EXE.d


git clone https://gitlab.com/libeigen/eigen.git;
mv eigen EigenLib;
mkdir data;
mkdir obj;
make;

COMMAND="./$EXE"
echo $COMMAND;
eval $COMMAND;
