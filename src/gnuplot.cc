
/*--------------------------------------------------------------------------

Author

    Mehdi Baba Mehdi
    mehdi.baba_mehdi@uni-wuppertal.de

Date  
    Fri 07.07.2020

\*-----------------------------------------------------------------------*/



#include "../includes/gnuplot.hpp"


/*____________________________ CONSTRUCTOR ____________________________________*/
template<class T>
gnuplot<T>::gnuplot()
{
   std::ofstream gplotFileStream(gnuplotFile);

   if (!gplotFileStream.is_open()) 
   {
      std::cerr << "==> Gnuplot class, File(s) failed to open" << std::endl;
      exit(0);
   }

   gplot << "set output './data/off_A.png'\n";
   gplot << "set terminal png\n";
   gplot << "set title \"off(A) vs. number of iteration in SYMMEIG algorithm\"\n";
   gplot << "set xlabel \"iteration number \" \n";
   gplot << "set ylabel \"log(||off(A)||)\" \n";
   gplot << "set logscale y \n";
   gplot << "set terminal png size 1024,768\n";
   gplot << "set palette rgb 33,13,10\n";
   gplot << "plot \"" << dataFile << "\" u 1:2 with linespoint\n";
   gplot << "set terminal x11\n";
   gplot << "set output\n";
   gplot << "replot\n";

   std::string cmdStr = gplot.str();
   gplotFileStream << cmdStr;
   gplotFileStream.close();

}


/*______________________________ DESTRUCTOR ____________________________________*/
template<class T>
gnuplot<T>::~gnuplot()
{
  system("rm -f ./data/data.dat");
  system("rm -f ./data/plot.gnu");
}


/*____________________________ MEMBER FUNCTION __________________________________*/
/*_____________________________________________________________________
 * @name  plot
 * @info The member function  plots given std::vector vs iteration numbers 
 *___________________________________________________________________*/
template<class T>
void gnuplot<T>::plot(const std::vector<T> vector_off_A, const int& iterationNumber)
{
   dataFileStream.open(dataFile);

   std::cout << "==> plotting result (./data/off_A.png)" << std::endl;
   if (!dataFileStream.is_open()) 
   {
      std::cerr << "==> Gnuplot class, File(s) failed to open" << std::endl;
      exit(0);
   }

   for (int i = 0; i < iterationNumber; ++i)
   {
     dataFileStream << i << " " << vector_off_A[i] << "\n" ;
   }

   dataFileStream.close();
   system("gnuplot --persist ./data/plot.gnu");

}
