
/*--------------------------------------------------------------------------

Author

    Mehdi Baba Mehdi
    mehdi.baba_mehdi@uni-wuppertal.de

Date
    Fri 08.07.2020

\*-----------------------------------------------------------------------*/




#ifndef EIGENVALUEOFMATRIX_HPP
#define EIGENVALUEOFMATRIX_HPP


#include <iostream>
#include <fstream>
#include <cmath>
#include <chrono>
#include <iomanip>
#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <Eigen/Core>
#include <Eigen/SVD>
#include "gnuplot.hpp"


template<class T>
class eigenValueOfSymMatrix;

template<class T>
std::ofstream& operator<< (std::ofstream& ofs, const eigenValueOfSymMatrix<T>& eigClass);

template<class T>
std::ostream& operator<< (std::ostream& ios, const eigenValueOfSymMatrix<T>& eigClass);


/*************************************************************\
  @name orthMatrix
  @info A structure used to store row, col (p,q) of 
        largest element of matrix A, and calculated c, s
        value for the element by "rotate" function in 
        "eigenValueOfSymMatrix" class
\************************************************************/      
template<class T>
struct orthMatrix
{
  int p;
  int q;
  T c;
  T s;
}; 



/*************************************************************\
  @name eigenValueOfSymMatrix
  @info A class to calculate eigenvalues and eigenvector of
        symmetric matrix "A" iteratively.
      
  @input _plotting: a Boolean variable in constructors used to 
                    activate/deactivate plotting using gnuplot 
                    class        
\************************************************************/      

template<class T>
class eigenValueOfSymMatrix
{
  private:
    int dim;
    orthMatrix<T> eigSymOrthMatrix;  
    gnuplot<T>* eigSymmGnuplot;
    Eigen::Matrix<T,Eigen::Dynamic, Eigen::Dynamic> A;
    Eigen::Matrix<T,Eigen::Dynamic, Eigen::Dynamic> D;
    Eigen::Matrix<T,Eigen::Dynamic, Eigen::Dynamic> Q;

    // used to activate/deactivate plotting using gnuplot class
    bool plotting = false;
    double cond_A = 0.0;

  public:
    eigenValueOfSymMatrix(std::ifstream& _fileName, int _dim, bool _plotting=false);
    
    eigenValueOfSymMatrix(const T* _A, int _dim, bool _plotting=false);
    
    eigenValueOfSymMatrix(const Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>& _A, 
                          bool _plotting=false);
    
    ~eigenValueOfSymMatrix();
    
    void findMaxOfMatrixD();
 
    T off();
    
    Eigen::SparseMatrix<T> createMatrixJ();
    
    void rotate();
    
    void symmEig(T tol);
    
    void eigenValues();
    
    double checkResult();
    
    void printResult(T off_A, double totalTime, double timeMMM, double timeRotate,
                     double timeOff, double timeMax, int count);

    // operator overloading to write eigenvectors of matrix in a given file or on terminal 
    friend std::ofstream& operator<< <>(std::ofstream& ofs, 
                                        const eigenValueOfSymMatrix<T>& eigClass);
    friend std::ostream& operator<< <>(std::ostream& ios, 
                                       const eigenValueOfSymMatrix<T>& eigClass);

};

#endif
