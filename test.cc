

#include "./includes/gnuplot.hpp"
#include "./src/gnuplot.cc"
#include "./includes/symmEigen.hpp"
#include "./src/symmEigen.cc"
#include <Eigen/Dense>
#include <Eigen/Core>
#include <stdlib.h>
#include <time.h>
#include <cmath>
#include <string>
#include <fstream>

int main()
{
   std::string matrixType;
   bool print;
   int dim;
   double tol = 1.0e-15;
   std::string QfileName = "./data/Q.dat";  // A file to store eigenvectors
   std::ofstream Qfile(QfileName);

   std::cout << "Select type of symmetric matrix, discrete Laplacian matrix (l), Random-filled matrix (r)" <<std::endl;
   std::cout << "type = ";
   std::cin >> matrixType;
   std::cout << "dim = ";
   std::cin >> dim;

   Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> matrix(dim, dim);

   if (matrixType == "l")
   {
      // discrete Laplacian matrix with entries -1, 2, -1
      matrix = Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic>::Identity(dim, dim);
      // set diagonal element to 2.0
      matrix *= 2.0;

      for (int i = 0; i < dim; ++i)
      {
         if (i < dim - 1) matrix(i,i+1) = -1.0;
         if (i > 0)matrix(i,i-1) = -1.0;
      }
   } else if (matrixType == "r")
   {
      // Random-filled matrix
      srand(time(NULL));
      for (int i = 0; i < dim; ++i)
      {
         matrix(i,i) = rand() % 10;
         for (int j = 0; j < i; ++j)
         {
            matrix(i,j) = rand() % 10;
            matrix(j,i) = matrix(i,j);
         }
      }
   } else
   {
     std::cerr << " Wrong input!" << std::endl;
     exit(-1);
   }

   eigenValueOfSymMatrix<double> eigenSymObject(matrix, true);
   eigenSymObject.symmEig(tol);
   
   std::cout << "Print eigenvalue of given matrix? yes(1), no(0)" << std::endl;
   std::cin >> print;
   
   if (print)
   {
     eigenSymObject.eigenValues();

     if (matrixType == "l")
     {
        std::cout << "==> Correct eigenvalues: " << std::endl;
        for (int i = 0; i < dim; ++i)
        {
            std::cout << std::setw(7) << std::left << "lambda(" << i + 1 << ") = ";
            std::cout << 2.0 * (1.0 - cos((i+1) * M_PI / (dim + 1.0))) << std::endl;
        }
        std::cout << std::endl;
     }
     std::cout << eigenSymObject;
   }

   // Write eigenvectors to the given file
   Qfile << eigenSymObject;
}
